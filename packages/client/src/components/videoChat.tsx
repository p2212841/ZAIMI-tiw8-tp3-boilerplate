import React, { useState, useRef } from "react";
import { AppDispatch } from '../store'
import { setLocalStream } from "../slices/board-slice";
import { useDispatch } from 'react-redux';

export const VideoChat: React.FC = () => {
    const [startAvailable, setStart] = useState(true);
    const [callAvailable, setCall] = useState(false);
    const [hangupAvailable, setHangup] = useState(false);

    const localVideoRef = useRef<HTMLVideoElement>(null);
    const remoteVideoRef = useRef<HTMLVideoElement>(null);

    const dispatch = useDispatch<AppDispatch>()

    
    const start = () => {
        navigator.mediaDevices
          .getUserMedia({
            audio: true,
            video: true,
          })
          .then(gotStream)
          .catch((e) => {
            console.log(e);
            alert("getUserMedia() error:" + e.name);
          });
      };
      
      const gotStream = (stream : MediaStream) => {
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = stream; // on injecte le flux vidéo local dans l'element video qui a pour ref 'localVideoRef'
            dispatch(setLocalStream(stream, false)); // sera utile plus tard pour avoir accès au flux dans le middleware
        }
    };
      
      const call = () => {
      
        setCall(false);
        setHangup(true);
      };

      const gotRemoteStream = (remoteStream : MediaStream) => {
        const remoteVideo = remoteVideoRef.current;
      
        if (remoteVideo) {
          remoteVideo.srcObject = remoteStream;
        }
      };

    const hangup = () => {
        if (localVideoRef.current) {
            const stream = localVideoRef.current.srcObject as MediaStream;
            if (stream) {
                const tracks = stream.getTracks();
                tracks.forEach((track) => {
                    track.stop();
                });
            }
        }
        setStart(true);
        setCall(false);
        setHangup(false);
    };

    return (
      <div>
        <div>
          <video
            ref={localVideoRef}
            autoPlay
            muted
            style={{ width: "40%" }}
          >
            <track kind="captions" srcLang="en" label="english_captions" />
          </video>

          <video
            ref={remoteVideoRef}
            autoPlay
            style={{ width: "40%" }}
          >
            <track kind="captions" srcLang="en" label="english_captions" />
          </video>
        </div>
        <div>

          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-2 w-10 h-10 mr-4 rounded-full button" onClick={start} disabled={!startAvailable}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                <path fill-rule="evenodd" d="M12 2.25a.75.75 0 01.75.75v9a.75.75 0 01-1.5 0V3a.75.75 0 01.75-.75zM6.166 5.106a.75.75 0 010 1.06 8.25 8.25 0 1011.668 0 .75.75 0 111.06-1.06c3.808 3.807 3.808 9.98 0 13.788-3.807 3.808-9.98 3.808-13.788 0-3.808-3.807-3.808-9.98 0-13.788a.75.75 0 011.06 0z" clip-rule="evenodd" />
            </svg>
          </button>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-2 w-10 h-10 mr-4 rounded-full button" onClick={call} disabled={!callAvailable}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                <path fill-rule="evenodd" d="M1.5 4.5a3 3 0 013-3h1.372c.86 0 1.61.586 1.819 1.42l1.105 4.423a1.875 1.875 0 01-.694 1.955l-1.293.97c-.135.101-.164.249-.126.352a11.285 11.285 0 006.697 6.697c.103.038.25.009.352-.126l.97-1.293a1.875 1.875 0 011.955-.694l4.423 1.105c.834.209 1.42.959 1.42 1.82V19.5a3 3 0 01-3 3h-2.25C8.552 22.5 1.5 15.448 1.5 6.75V4.5z" clip-rule="evenodd" />
            </svg>
          </button>
          <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-2 w-10 h-10 rounded-full button" onClick={hangup} disabled={!hangupAvailable}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                <path fill-rule="evenodd" d="M5.47 5.47a.75.75 0 011.06 0L12 10.94l5.47-5.47a.75.75 0 111.06 1.06L13.06 12l5.47 5.47a.75.75 0 11-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 01-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 010-1.06z" clip-rule="evenodd" />
            </svg>
          </button>
        </div>
      </div>
    );
  }
