import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@img': path.resolve('./public/img'),
      "simple-peer": "simple-peer/simplepeer.min.js",
    },
  },
  plugins: [react()],
})
