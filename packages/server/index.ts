import express from 'express';
import { Socket } from 'socket.io'

const app = express()
const port = process.env.PORT || 3000
const path = require('path')
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)

app.get('/data', (req, res) => {
  res.json({ foo: 'bar' })
})

const DIST_DIR = path.join(__dirname, '../../client/dist')
const HTML_FILE = path.join(DIST_DIR, 'index.html')

server.listen(port, () => {
  console.log(`Server started on port: ${port}\n`)
})

app.use(express.static(DIST_DIR))
app.get('/*', (req, res) => {
  res.sendFile(HTML_FILE)
})
let ini = true
io.on('connection', (socket: Socket) => {
  console.log('Client connected with ', socket.id)
  ini = !ini
  socket.emit('peer', {
    peerId: socket.id,
    initiator: ini, // Set the initiator property to true for the client that initiated the connection
  })

  socket.on("signal", (data) => {
    console.log(data)
    socket.broadcast.emit("signal", {
      signal:data.signal,
      peerId: socket.id
    });
  })

  socket.on('disconnect', () => {
    console.log('Client disconnected')
  })
})
