import { configureStore } from '@reduxjs/toolkit'
import { AnyAction, Dispatch, Middleware } from "redux";
import { logger } from 'redux-logger'
import boardReducer from '../slices/board-slice'
import { io } from 'socket.io-client'
import SimplePeer from 'simple-peer'

const socket = io()
console.log(socket)
let globalPeer: SimplePeer.Instance;

socket.on('connection', function () {
  console.log('connection ====>')
})

// Réception de données du serveur, y compris l'ID du pair et le booléen initiateur
socket.on('peer', (data) => {
  const { peerId, initiator } = data
  console.log(`Peer connected : peer ${peerId}. Initiator: ${initiator}`)

  const useTrickle = true
  const peer: SimplePeer.Instance = new SimplePeer({
    initiator: initiator,
    trickle: useTrickle, // useTrickle doit être a true pour que le peer persiste
    config: {
      iceServers: [
        { urls: 'stun:stun.l.google.com:19302' },
        { urls: 'stun:global.stun.twilio.com:3478' },
      ],
    },
  })

  globalPeer=peer;

  console.log('This is peer : ', peer)

  if (peer) {
    //cet événement est déclenché lorsqu'un signal est reçu sur la socket. La fonction associée doit propager ce signal au peer.
    socket.on('signal', function (data) {
      console.log('received signal on socket')
      if (peer) {
        peer.signal(data.signal)
      }
    })

    peer.on('signal', function (data) {
      console.log(
        'Advertising signaling data',
          data ,
          'to Peer ID:' ,
          peerId
      )
      socket.emit('signal', {
        signal: data,
        peerId: socket.id,
      })
    })

    //cet événement est déclenché lorsqu'une erreur survient lors de l'envoi d'une connexion au peer.
    peer.on('error', function (e) {
      console.log('Error sending connection to peer :' + peerId + ':' + e)
    })

    //cet événement est déclenché lorsque la connexion avec le peer est établie avec succès. La fonction associée peut envoyer des données au peer avec la méthode send().
    peer.on('connect', function () {
      console.log('Peer connection established with Peer ID: ' + peerId)
      peer.send(JSON.stringify({ message: 'Hello from peer ' + peerId }))
    })

    //cet événement est déclenché lorsqu'une donnée est reçue du peer. La fonction associée doit prendre les mesures appropriées pour traiter les données reçues.
    peer.on('data', function (data) {
      console.log('Received data from peer : ' + data)
      // les donnees arrivent sous forme de string,
      // si le string est un objet JSON serialise avec JSON.stringify(objet)
      // JSON.parse(string) permet de reconstruire l'objet
      const restructuredData = JSON.parse(data);
      console.log ("data : " + restructuredData)
      restructuredData.meta.propagate = false;
      if(restructuredData.type === 'board/setAvatar' || restructuredData.type === 'board/movePlayer') {
        restructuredData.payload[1] = 'remote';
        store.dispatch(restructuredData);
    }
      store.dispatch(restructuredData);
    })

    //cet événement est déclenché lorsqu'un flux de données est reçu du peer.
    peer.on('stream', (stream): void => {
      console.log('got stream ' + stream)
      const video = document.querySelector('video')
      if (video){
        video.srcObject = stream
        video.play()
    
        document.body.appendChild(video)
      }
    })

  }
})

const myLoggerMiddleware: Middleware<Dispatch> = (api) => (next) => {
  return (restructuredData: AnyAction) => {
      const remotePeer ={
        currentPosition : api.getState().playerPosition,
        currentAvatar : api.getState().playerAvatar
      }
      if(restructuredData.meta.propagate)
                globalPeer.send(JSON.stringify(restructuredData));
      return next(restructuredData);
  };
};

export const store = configureStore({
    reducer: boardReducer,
    middleware: [logger,myLoggerMiddleware],
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
