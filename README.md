# tiw8-tp3-boilerplate

Sujet : https://aurelient.github.io/tiw8/2022/TP3/

Boilerplate inpired from : https://dev.to/alexeagleson/how-to-create-a-node-and-react-monorepo-with-git-submodules-2g83

L'objectif est de réaliser un clone simplifié de [Gathertown](https://www.gather.town/).


## Etat du projet :
Le projet se lance sans aucun problème remonté. Il présente une map avec des joueurs qui se déplacent.
Pour l'instant, le lancement d'une seule interface présente un problème. Il faut donc lancer deux interfaces au même temps pour pouvoir établir la connexion entre les deux peers.

Une fois les deux interfaces sont lancé, le type de chaque Avatar et leurs positions sont partagés entre les deux peers et bien synchonisés, on voit alors la position du deuxième Avatar à partir de l'interface du premier.

Pour la fonction ChatVideo, nous avons pu jusqu'à présent activer la fonction "start" qui permet d'activer l'appel video d'un joueur.

## Dépendances :

### Serveur 
    development dependencies : 
        express : 4.18.2,
        typescript : 4.9.5,
        socket.io : 4.6.1,
        simple-peer : 9.11.1,
        @types/express : 4.17.17,
        @types/simple-peer : 9.11.5,

### Client
    development dependencies :
        @reduxjs/toolkit: 1.9.2,
        @types/simple-peer: 9.11.5,
        autoprefixer: 10.4.13,
        events: 3.3.0,
        postcss: 8.4.21,
        process: 0.11.10,
        randombytes: 2.1.0,
        react: 18.2.0,
        react-dom: 18.2.0,
        react-redux: 8.0.5,
        redux-logger: 3.0.6,
        simple-peer: 9.11.1,
        socket.io-client: 4.6.1,
        tailwindcss: 3.2.7,
        @types/react: 18.0.27,
        @types/react-dom: 18.0.10,
        @types/redux-logger: 3.0.9,
        @vitejs/plugin-react-swc: 3.0.0,
        typescript: 4.9.5,
        vite: 4.1.0,

### Dépendences globales
    development dependencies :
        lerna: 6.5.1,
        typescript: 4.9.5,
        globalthis: 1.0.3,
        randombytes: 2.1.0,

## Build 

### Utilisation :
        yarn       : pour installer les dépendances
        yarn build : pour le build du client (cd client)
        yarn dev   : pour lancer le serveur  (cd server)

Pour installer des dependances spécifiques au client ou server il faut dabord cd dans dans la repertoire cible avant de `yarn add LE_PACKAGE`

## Picture
![ Capture du résultat ](https://forge.univ-lyon1.fr/p2212841/ZAIMI-tiw8-tp3-boilerplate/-/blob/main/Capture.PNG)
